package in.greendev.logistics.model;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import com.sun.org.apache.xpath.internal.operations.Bool;
import org.hibernate.validator.constraints.NotBlank;

@MappedSuperclass
public abstract class Vehical {

    @GeneratedValue
    @Id
    private Long id;

    @NotBlank
    private String name;

    private Boolean sold;

    public Vehical() {
    }

    public Vehical(Long id, String name, Boolean sold) {
        this.id = id;
        this.name = name;
        this.sold = sold;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getSold() {
        return sold;
    }

    public void setSold(Boolean sold) {
        this.sold = sold;
    }
}
