package in.greendev.logistics.feature.shipment.downloadAndUpload;

import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


@CrossOrigin(origins = {"https://plewinski-logistics.com", "https://www.plewinski-logistics.com", "http://localhost:4200"})
@RestController
public class DownloadAndUploadApi {

    private DownloadAndUploadService downloadAndUploadService;

    public DownloadAndUploadApi(DownloadAndUploadService downloadAndUploadService) {
        this.downloadAndUploadService = downloadAndUploadService;
    }

    @RequestMapping(value = "/driver/api/v1/shipments/upload", method = RequestMethod.POST)
    public ResponseEntity<?> singleFileUpload(MultipartFile file) {
        return new ResponseEntity<>(
                this.downloadAndUploadService.singleFileUpload(file), HttpStatus.OK);
    }

    @RequestMapping(value = "/driver/api/v1/shipments/download/{name:.+}", method = RequestMethod.GET)
    public ResponseEntity<Resource> singleFileDownloadForDriver(@PathVariable("name") final String name) throws IOException {
        System.out.println(name);

        return this.downloadAndUploadService.singleFileDownload(name);
    }

    @RequestMapping(value = "/customer-one/api/v1/shipments/download/{name:.+}", method = RequestMethod.GET)
    public ResponseEntity<Resource> singleFileDownloadForCustomer(@PathVariable("name") final String name) throws IOException {
        System.out.println(name);

        return this.downloadAndUploadService.singleFileDownload(name);
    }
}
