package in.greendev.logistics.feature.shipment.forDriverAndManager;

import in.greendev.logistics.model.Shipment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface IShipmentDaoForDriverAndManager extends JpaRepository<Shipment, Long> {

    @Query("SELECT s FROM Shipment s WHERE s.shipmentNumber = :number OR s.salesOrder = :number")
    List<Shipment> getShipmentsByShipmentNumberSalesOrder(@Param("number") final String number);

    @Query("SELECT s FROM Shipment s WHERE  LOWER(s.place) like LOWER(CONCAT('%', :word, '%')) OR LOWER(s.name) like LOWER(CONCAT('%', :word, '%'))")
    List<Shipment> getShipmentsFromSearchByWord(@Param("word") final String word);

    @Query("SELECT s FROM Shipment s INNER JOIN s.drivers d WHERE d.driverName = :driverName AND (s.shipmentNumber = :number OR s.salesOrder = :number)")
    List<Shipment> getShipmentsByShipmentNumberSalesOrderAndDriverName(@Param("number") final String number, @Param("driverName") final String driverName);

    //    @Query(value = "SELECT * FROM shipment s LEFT JOIN (SELECT DISTINCT k.shipment_id FROM shipment_scan_links k) AS foo ON s.id = foo.shipment_id ORDER BY foo NULLS FIRST, s.loading_date DESC, s.unloading_date DESC, ?#{#pageable}", countQuery = "SELECT count(*) FROM shipment", nativeQuery = true)
    @Query(value = "SELECT * FROM shipment s LEFT JOIN (SELECT DISTINCT k.shipment_id FROM shipment_scan_links k) AS foo ON s.id = foo.shipment_id ORDER BY s.loading_date DESC, s.unloading_date DESC, foo NULLS FIRST, ?#{#pageable}", countQuery = "SELECT count(*) FROM shipment", nativeQuery = true)
//    @Query(value = "SELECT * FROM shipment s LEFT JOIN (SELECT DISTINCT k.shipment_id FROM shipment_scan_links k) AS foo ON s.id = foo.shipment_id WHERE foo IS NULL ORDER BY foo NULLS FIRST AND WHERE foo IS NOT NULL ORDER BY s.loading_date DESC, s.unloading_date DESC,  ?#{#pageable}", countQuery = "SELECT count(*) FROM shipment", nativeQuery = true)
    Page<Shipment> getAllByPageInCustomDirection(Pageable pageable);


    Page<Shipment> findByLoadingDateBetween(LocalDate startDate, LocalDate endDate, Pageable pageable);

    @Query("SELECT DISTINCT s FROM Shipment s LEFT OUTER JOIN s.dutyShipment d LEFT OUTER JOIN s.customers c LEFT OUTER JOIN s.drivers dr WHERE (d.dutyType IN :dutyShipmentType OR d IS NULL) AND (c.customerName IN :customerNames OR c IS NULL) AND (dr.driverName IN :driverNames OR dr IS NULL) AND (:name IS NULL OR LOWER(s.name) LIKE :name) AND (:place IS NULL OR LOWER(s.place) LIKE :place) AND ((s.loadingDate BETWEEN :startLoading AND :endLoading) OR s.loadingDate IS NULL)  AND ((s.unloadingDate BETWEEN :startUnloading AND :endUnloading) OR s.unloadingDate IS NULL)")
    Page<Shipment> findByAllParameters(@Param("dutyShipmentType") List<String> dutyShipmentType, @Param("customerNames") List<String> customerNames, @Param("driverNames") List<String> driverNames, @Param("name") String name, @Param("place") String place, @Param("startLoading") LocalDate startLoading, @Param("endLoading") LocalDate endLoading, @Param("startUnloading") LocalDate startUnloading, @Param("endUnloading") LocalDate endUnloading, Pageable pageable);
}


