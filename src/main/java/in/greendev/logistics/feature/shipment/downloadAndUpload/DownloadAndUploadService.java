package in.greendev.logistics.feature.shipment.downloadAndUpload;


import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;

@Service
@Transactional
public class DownloadAndUploadService {

    //    private static String UPLOADED_FOLDER = "/usr/home/mateusz/domains/plewinski.greendev.in/";
    private final String UPLOADED_FOLDER = "/usr/home/plewinski/documentsFromApplication/";
//    private static String UPLOADED_FOLDER = "/Users/mateuszzielinski/Projects/";

    public String singleFileUpload(MultipartFile file) {
        Path path = null;
        long time = System.currentTimeMillis();
        try {
            // Get the file and save it somewhere
            byte[] bytes = file.getBytes();
            path = Paths.get(UPLOADED_FOLDER + time + "_" + file.getOriginalFilename());
            Files.write(path, bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return time + "_" + file.getOriginalFilename();
    }

    public ResponseEntity<Resource> singleFileDownload(final String name) throws IOException {

        File file2Upload = new File(this.UPLOADED_FOLDER + name);
        Path path = Paths.get(this.UPLOADED_FOLDER + name);

        ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));

        HttpHeaders headers = new HttpHeaders();
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        headers.add("Access-Control-Allow-Origin", "https://plewinski-logistics.com");

        return ResponseEntity.ok()
                .headers(headers)
                .contentLength(file2Upload.length())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);
    }


}
