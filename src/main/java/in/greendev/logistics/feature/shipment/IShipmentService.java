package in.greendev.logistics.feature.shipment;

import in.greendev.logistics.model.Shipment;
import in.greendev.logistics.model.mappers.ShipmentForPublican;
import in.greendev.logistics.service.ICrudService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IShipmentService extends ICrudService<Shipment> {

    Page<Shipment> findAll(Pageable pageable);
}
