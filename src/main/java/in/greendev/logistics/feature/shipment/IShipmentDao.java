package in.greendev.logistics.feature.shipment;

import in.greendev.logistics.model.Shipment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IShipmentDao extends JpaRepository<Shipment, Long> {
}
