package in.greendev.logistics.feature.shipment.forPublican;

import in.greendev.logistics.model.Shipment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IShipmentDaoForPublican extends JpaRepository<Shipment, Long>{

    @Query("SELECT s FROM Shipment s INNER JOIN s.customers c WHERE c.customerName = :customerName AND (s.shipmentNumber = :number)")
    List<Shipment> getShipmentsForPublicanByShipmentNumberAndCustomerName(@Param("number") final String number, @Param("customerName") final String customerName);

    @Query("SELECT s FROM Shipment s INNER JOIN s.customers c WHERE c.customerName = :customerName AND (LOWER(s.place) like LOWER(CONCAT('%', :word, '%')) OR LOWER(s.name) like LOWER(CONCAT('%', :word, '%')))")
    List<Shipment> getShipmentsFromSearchByWordForPublican(@Param("word") final String word, @Param("customerName") final String customerName);


}
