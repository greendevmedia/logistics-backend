package in.greendev.logistics.feature.shipment.forDriverAndManager;

import in.greendev.logistics.model.Shipment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service
@Transactional
public class ShipmentServiceForDriverAndManager implements IShipmentServiceForDriverAndManager {

    private final IShipmentDaoForDriverAndManager shipmentDaoForDriverAndManager;

    public ShipmentServiceForDriverAndManager(IShipmentDaoForDriverAndManager shipmentDaoForDriverAndManager) {
        this.shipmentDaoForDriverAndManager = shipmentDaoForDriverAndManager;
    }

    @Override
    public List<Shipment> getShipmentsByShipmentNumberSalesOrder(String number) {
        return shipmentDaoForDriverAndManager.getShipmentsByShipmentNumberSalesOrder(number);
    }

    @Override
    public List<Shipment> getShipmentsFromSearchByWord(String word) {
        return shipmentDaoForDriverAndManager.getShipmentsFromSearchByWord(word);
    }

    @Override
    public List<Shipment> getShipmentsByShipmentNumberSalesOrderAndDriverName(String number, String driverName) {
        return shipmentDaoForDriverAndManager.getShipmentsByShipmentNumberSalesOrderAndDriverName(number, driverName);
    }

    @Override
    public Page<Shipment> getAllByPageInCustomDirection(Pageable pageable) {
        return shipmentDaoForDriverAndManager.getAllByPageInCustomDirection(pageable);
    }

    @Override
    public Page<Shipment> findByLoadingDateBetween(LocalDate startDate, LocalDate endDate, Pageable pageable) {
        return shipmentDaoForDriverAndManager.findByLoadingDateBetween(startDate, endDate, pageable);
    }

    @Override
    public Page<Shipment> findByAllParameters(List<String> dutyShipmentType, List<String> customerNames, List<String> driverNames,String name, String place, LocalDate startLoading, LocalDate endLoading, LocalDate startUnloading, LocalDate endUnloading, Pageable pageable) {
        if (name != null) {
            name = "%" + name.toLowerCase() + "%";
        }
        if (place != null) {
            place = "%" + place.toLowerCase() + "%";
        }
        return shipmentDaoForDriverAndManager.findByAllParameters(dutyShipmentType, customerNames, driverNames, name, place, startLoading, endLoading, startUnloading, endUnloading, pageable);
    }
}
