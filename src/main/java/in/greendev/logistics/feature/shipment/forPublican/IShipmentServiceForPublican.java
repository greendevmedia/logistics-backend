package in.greendev.logistics.feature.shipment.forPublican;

import in.greendev.logistics.model.mappers.ShipmentForCustomer;
import in.greendev.logistics.model.mappers.ShipmentForPublican;

import java.util.List;

public interface IShipmentServiceForPublican {

    List<ShipmentForPublican> getShipmentsForPublicanByShipmentNumberAndCustomerName(String number, String customerName);

    List<ShipmentForPublican> getShipmentsFromSearchByWordForPublican(String word, String customerName);
}
