package in.greendev.logistics.feature.shipment.forCustomers;

import in.greendev.logistics.model.Shipment;
import in.greendev.logistics.model.mappers.ShipmentForCustomer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"https://plewinski-logistics.com", "https://www.plewinski-logistics.com", "http://localhost:4200"})
@RestController
public class ShipmentApiForCustomer {

    private final IShipmentServiceForCustomer shipmentServiceForCustomer;

    public ShipmentApiForCustomer(IShipmentServiceForCustomer shipmentServiceForCustomer) {
        this.shipmentServiceForCustomer = shipmentServiceForCustomer;
    }

    @RequestMapping(value = "/customer-one/api/v1/shipments/{customerName}/{number}", method = RequestMethod.GET)
    public ResponseEntity<?> getShipmentsByShipmentNumberSalesOrderAndCustomerName(@PathVariable("number") final String number, @PathVariable("customerName") final String customerName) {
        final List<ShipmentForCustomer> shipmentsForCustomer = shipmentServiceForCustomer.getShipmentsByShipmentNumberSalesOrderAndCustomerName(number, customerName);
        HttpStatus httpStatus = HttpStatus.OK;
        if (shipmentsForCustomer.size() == 0) {
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<>(shipmentsForCustomer, httpStatus);
    }

    @RequestMapping(value = "/customer-all/api/v1/shipments/{customerName}/shipments-limit", method = RequestMethod.GET)
    Page<ShipmentForCustomer> listForCustomer(Pageable pageable, @PathVariable("customerName") String customerName) {
        final Page<ShipmentForCustomer> shipmentPage = shipmentServiceForCustomer.listAllByPageForCustomerByName(pageable, customerName);
        return shipmentPage;
    }

    @RequestMapping(value = "/customer-one/api/v1/shipments/{customerName}/shipments-limit", method = RequestMethod.GET)
    Page<ShipmentForCustomer> listForCustomerLimit15(Pageable pageable, @PathVariable("customerName") String customerName) {
        final Page<ShipmentForCustomer> shipmentPage = shipmentServiceForCustomer.listAllByPageForCustomerByName(pageable, customerName);
        return shipmentPage;
    }

    @RequestMapping(value = "/customer-one/api/v1/shipments/searching/{customerName}/{word}", method = RequestMethod.GET)
    public ResponseEntity<?> byWordSearchForCustomer(@PathVariable("word") final String word, @PathVariable("customerName") final String customerName) {
        final List<ShipmentForCustomer> shipmentForCustomers = shipmentServiceForCustomer.getShipmentsFromSearchByWordForCustomer(word, customerName);
        HttpStatus httpStatus = HttpStatus.OK;
        if (shipmentForCustomers.size() == 0) {
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<>(shipmentForCustomers, httpStatus);
    }
}
