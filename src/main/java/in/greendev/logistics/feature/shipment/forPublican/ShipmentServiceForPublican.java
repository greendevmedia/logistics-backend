package in.greendev.logistics.feature.shipment.forPublican;

import in.greendev.logistics.model.Shipment;
import in.greendev.logistics.model.mappers.ShipmentForCustomer;
import in.greendev.logistics.model.mappers.ShipmentForPublican;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class ShipmentServiceForPublican implements IShipmentServiceForPublican {

    private final IShipmentDaoForPublican shipmentDaoForPublican;

    public ShipmentServiceForPublican(IShipmentDaoForPublican shipmentDaoForPublican) {
        this.shipmentDaoForPublican = shipmentDaoForPublican;
    }

    @Override
    public List<ShipmentForPublican> getShipmentsForPublicanByShipmentNumberAndCustomerName(String number, String customerName) {
        ModelMapper modelMapper = new ModelMapper();
        List<ShipmentForPublican> shipmentForPublicans = new ArrayList<>();
        List<Shipment> shipments = shipmentDaoForPublican.getShipmentsForPublicanByShipmentNumberAndCustomerName(number, customerName);
        for (Shipment shipment : shipments) {
            shipmentForPublicans.add(modelMapper.map(shipment, ShipmentForPublican.class));
        }
        return shipmentForPublicans;
    }


    @Override
    public List<ShipmentForPublican> getShipmentsFromSearchByWordForPublican(String word, String customerName) {
        ModelMapper modelMapper = new ModelMapper();
        List<ShipmentForPublican> shipmentForPublicans = new ArrayList<>();
        List<Shipment> shipments = shipmentDaoForPublican.getShipmentsFromSearchByWordForPublican(word, customerName);
        for (Shipment shipment : shipments) {
            shipmentForPublicans.add(modelMapper.map(shipment, ShipmentForPublican.class));
        }
        return shipmentForPublicans;
    }
}
