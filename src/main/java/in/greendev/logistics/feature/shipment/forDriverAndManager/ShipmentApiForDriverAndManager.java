package in.greendev.logistics.feature.shipment.forDriverAndManager;

import in.greendev.logistics.model.Shipment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@CrossOrigin(origins = {"https://plewinski-logistics.com", "https://www.plewinski-logistics.com", "http://localhost:4200"})
@RestController
public class ShipmentApiForDriverAndManager {

    private final IShipmentServiceForDriverAndManager shipmentServiceForDriverAndManager;

    public ShipmentApiForDriverAndManager(IShipmentServiceForDriverAndManager shipmentServiceForDriverAndManager) {
        this.shipmentServiceForDriverAndManager = shipmentServiceForDriverAndManager;
    }

    @RequestMapping(value = "/driver/api/v1/shipments/ship/{number}", method = RequestMethod.GET)
    public ResponseEntity<?> byShipmentNumberSalesOrder(@PathVariable("number") final String number) {
        final List<Shipment> shipments = shipmentServiceForDriverAndManager.getShipmentsByShipmentNumberSalesOrder(number);
        HttpStatus httpStatus = HttpStatus.OK;
        if (shipments.size() == 0) {
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<>(shipments, httpStatus);
    }

    @RequestMapping(value = "/driver/api/v1/shipments/ship/{driverName}/{number}", method = RequestMethod.GET)
    public ResponseEntity<?> byShipmentNumberSalesOrderForDrivers(@PathVariable("number") final String number, @PathVariable("driverName") final String driverName) {
        final List<Shipment> shipments = shipmentServiceForDriverAndManager.getShipmentsByShipmentNumberSalesOrderAndDriverName(number, driverName);
        HttpStatus httpStatus = HttpStatus.OK;
        if (shipments.size() == 0) {
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<>(shipments, httpStatus);
    }

    @RequestMapping(value = "/driver/api/v1/shipments/search/{word}", method = RequestMethod.GET)
    public ResponseEntity<?> byWordSearch(@PathVariable("word") final String word) {
        final List<Shipment> shipments = shipmentServiceForDriverAndManager.getShipmentsFromSearchByWord(word);
        HttpStatus httpStatus = HttpStatus.OK;
        if (shipments.size() == 0) {
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<>(shipments, httpStatus);
    }

    @RequestMapping(value = "/driver/api/v1/shipments/shipments-limit", method = RequestMethod.GET)
    Page<Shipment> getAllByPageInCustomDirection(Pageable pageable) {
        final Page<Shipment> shipmentPage = shipmentServiceForDriverAndManager.getAllByPageInCustomDirection(pageable);
        return shipmentPage;
    }

    @RequestMapping(value = "/driver/api/v1/shipments/loading-date/{startDateString}/{endDateString}", method = RequestMethod.GET)
    Page<Shipment> findByLoadingDateBetween(@PathVariable final String startDateString, @PathVariable final String endDateString, Pageable pageable) {
        final Page<Shipment> shipmentPage = shipmentServiceForDriverAndManager.findByLoadingDateBetween(LocalDate.parse(startDateString), LocalDate.parse(endDateString), pageable);
        return shipmentPage;
    }

    @RequestMapping(value = "/driver/api/v1/shipments/parameters", method = RequestMethod.GET)
    Page<Shipment> findByAllParameters(@RequestParam(value = "dutyType", defaultValue = "none,oral,paper") final List<String> parameters, @RequestParam(value = "customers", defaultValue = "csd pl,csd de,buk,marceli,ludo,mazury-trans,spedition-timocom") final List<String> customerNames, @RequestParam(value = "drivers", defaultValue = "włodzimierz fertyk,mirosław tecław,leszek tecław,krzysiek chojnacki,adam ciechański,paweł plewiński,adam wilczyński,tomasz czekała") final List<String> driversName, @RequestParam(value = "name", required = false) final String name, @RequestParam(value = "place", required = false) final String place, @RequestParam(value = "startLoading", required = false, defaultValue = "2000-01-01") String startLoading, @RequestParam(value = "endLoading", required = false,defaultValue = "2100-12-12") String endLoading, @RequestParam(value = "startUnloading", defaultValue = "2000-01-01", required = false) String startUnloading, @RequestParam(value = "endUnloading", defaultValue = "2100-12-12", required = false)String endUnloading, Pageable pageable) {
        final Page<Shipment> shipmentPage = shipmentServiceForDriverAndManager.findByAllParameters(parameters, customerNames, driversName, name, place, LocalDate.parse(startLoading), LocalDate.parse(endLoading), LocalDate.parse(startUnloading), LocalDate.parse(endUnloading), pageable);
        return shipmentPage;
    }
}
