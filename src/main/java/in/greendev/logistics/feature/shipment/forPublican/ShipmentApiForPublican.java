package in.greendev.logistics.feature.shipment.forPublican;

import in.greendev.logistics.model.mappers.ShipmentForCustomer;
import in.greendev.logistics.model.mappers.ShipmentForPublican;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"https://plewinski-logistics.com", "https://www.plewinski-logistics.com", "http://localhost:4200"})
@RestController
public class ShipmentApiForPublican {

    public final IShipmentServiceForPublican shipmentServiceForPublican;

    public ShipmentApiForPublican(IShipmentServiceForPublican shipmentServiceForPublican) {
        this.shipmentServiceForPublican = shipmentServiceForPublican;
    }

    @RequestMapping(value = "/publican/api/v1/shipments/{customerName}/{number}", method = RequestMethod.GET)
    public ResponseEntity<?> getShipmentsForPublicanByShipmentNumberAndCustomerName(@PathVariable("number") final String number, @PathVariable("customerName") final String customerName) {
        final List<ShipmentForPublican> shipmentForPublicans = shipmentServiceForPublican.getShipmentsForPublicanByShipmentNumberAndCustomerName(number, customerName);
        HttpStatus httpStatus = HttpStatus.OK;
        if (shipmentForPublicans.size() == 0) {
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<>(shipmentForPublicans, httpStatus);
    }


    @RequestMapping(value = "/publican/api/v1/shipments/searching/{customerName}/{word}", method = RequestMethod.GET)
    public ResponseEntity<?> byWordSearchForCustomer(@PathVariable("word") final String word, @PathVariable("customerName") final String customerName) {
        final List<ShipmentForPublican> shipmentForPublicans = shipmentServiceForPublican.getShipmentsFromSearchByWordForPublican(word, customerName);
        HttpStatus httpStatus = HttpStatus.OK;
        if (shipmentForPublicans.size() == 0) {
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<>(shipmentForPublicans, httpStatus);
    }
}
