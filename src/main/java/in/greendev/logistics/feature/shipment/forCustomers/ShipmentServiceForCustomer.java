package in.greendev.logistics.feature.shipment.forCustomers;

import in.greendev.logistics.model.Shipment;
import in.greendev.logistics.model.mappers.ShipmentForCustomer;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Service
@Transactional
public class ShipmentServiceForCustomer implements IShipmentServiceForCustomer {

    private final IShipmentDaoForCustomer shipmentDaoForCustomer;

    private final Sort SORT = new Sort(Sort.Direction.DESC, "id");

    public ShipmentServiceForCustomer(IShipmentDaoForCustomer shipmentDaoForCustomer) {
        this.shipmentDaoForCustomer = shipmentDaoForCustomer;
    }

    @Override
    public List<ShipmentForCustomer> getShipmentsByShipmentNumberSalesOrderAndCustomerName(String number, String customerName) {
        ModelMapper modelMapper = new ModelMapper();
        List<ShipmentForCustomer> shipmentsForCustomer = new ArrayList<>();
        List<Shipment> shipments = shipmentDaoForCustomer.getShipmentsByShipmentNumberSalesOrderAndCustomerName(number, customerName);
        for (Shipment shipment : shipments) {
            shipmentsForCustomer.add(modelMapper.map(shipment, ShipmentForCustomer.class));
        }
        return shipmentsForCustomer;
    }


    @Override
    public Page<ShipmentForCustomer> listAllByPageForCustomerByName(Pageable pageable, String customerName) {
        ModelMapper modelMapper = new ModelMapper();
        Pageable pageableSort = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), this.SORT);
        Page<Shipment> shipments = shipmentDaoForCustomer.listAllByPageForCustomerByName(pageableSort, customerName);
        List<ShipmentForCustomer> shipmentForCustomers = new ArrayList<>();
        for (Shipment shipment : shipments) {
            shipmentForCustomers.add(modelMapper.map(shipment, ShipmentForCustomer.class));
        }
        Page<ShipmentForCustomer> shipmentForCustomersPage = new PageImpl<ShipmentForCustomer>(shipmentForCustomers, pageableSort, shipments.getTotalElements());
        return shipmentForCustomersPage;
    }

    @Override
    public List<ShipmentForCustomer> getShipmentsFromSearchByWordForCustomer(String word, String customerName) {
        ModelMapper modelMapper = new ModelMapper();
        List<ShipmentForCustomer> shipmentsForCustomer = new ArrayList<>();
        List<Shipment> shipments = shipmentDaoForCustomer.getShipmentsFromSearchByWordForCustomer(word, customerName);
        for (Shipment shipment : shipments) {
            shipmentsForCustomer.add(modelMapper.map(shipment, ShipmentForCustomer.class));
        }
        return shipmentsForCustomer;
    }
}
