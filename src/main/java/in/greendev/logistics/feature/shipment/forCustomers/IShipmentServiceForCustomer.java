package in.greendev.logistics.feature.shipment.forCustomers;

import in.greendev.logistics.model.Shipment;
import in.greendev.logistics.model.mappers.ShipmentForCustomer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IShipmentServiceForCustomer {

    List<ShipmentForCustomer> getShipmentsByShipmentNumberSalesOrderAndCustomerName(String number, String customerName);

    Page<ShipmentForCustomer> listAllByPageForCustomerByName(Pageable pageable, String customerName);

    List<ShipmentForCustomer> getShipmentsFromSearchByWordForCustomer(String word, String customerName);
}
