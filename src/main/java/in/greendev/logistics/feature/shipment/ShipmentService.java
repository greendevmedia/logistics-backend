package in.greendev.logistics.feature.shipment;

import in.greendev.logistics.model.Shipment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ShipmentService implements IShipmentService {

    private final IShipmentDao shipmentDao;

    public ShipmentService(IShipmentDao shipmentDao) {
        this.shipmentDao = shipmentDao;
    }

    @Override
    public List<Shipment> findAll() {
        return shipmentDao.findAll();
    }

    @Override
    public Shipment getOne(Long id) {
        return shipmentDao.getOne(id);
    }

    @Override
    public void delete(Shipment shipment) {
        shipmentDao.delete(shipment);
    }

    @Override
    public void save(Shipment shipment) {
        shipmentDao.save(shipment);
    }

    @Override
    public void deleteById(Long id) {
        shipmentDao.delete(id);
    }

    private final Sort SORT = new Sort(Sort.Direction.DESC, "id");

    @Override
    public Page<Shipment> listAllByPage(Pageable pageable) {
        Pageable pageableSort = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), this.SORT);
        return shipmentDao.findAll(pageableSort);
    }

    public Page<Shipment> findAll(Pageable pageable){
        return shipmentDao.findAll(pageable);
    }


}
