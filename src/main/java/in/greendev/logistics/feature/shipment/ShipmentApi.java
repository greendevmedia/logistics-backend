package in.greendev.logistics.feature.shipment;

import in.greendev.logistics.model.Shipment;
import in.greendev.logistics.model.mappers.ShipmentForPublican;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = {"https://plewinski-logistics.com", "https://www.plewinski-logistics.com", "http://localhost:4200"})
@RestController
public class ShipmentApi {

    private final IShipmentService shipmentService;

    public ShipmentApi(IShipmentService shipmentService) {
        this.shipmentService = shipmentService;
    }

    @RequestMapping(value = "/driver/api/v1/shipments", method = RequestMethod.GET)
    public ResponseEntity<?> list() {
        final List<Shipment> shipments = shipmentService.findAll();
        return new ResponseEntity<>(shipments, HttpStatus.OK);
    }

    @RequestMapping(value = "/driver/api/v1/shipments", method = RequestMethod.POST)
    public ResponseEntity<?> save(@RequestBody final Shipment shipment) {
        shipmentService.save(shipment);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/driver/api/v1/shipments", method = RequestMethod.PUT)
    public ResponseEntity<?> update(@RequestBody final Shipment shipment) {
        shipmentService.save(shipment);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/driver/api/v1/shipments/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> delete(@PathVariable("id") final Long id) {
        shipmentService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/driver/api/v1/shipments/limit", method = RequestMethod.GET)
    Page<Shipment> findAll(Pageable pageable) {
        final Page<Shipment> shipmentPage = shipmentService.findAll(pageable);
        return shipmentPage;
    }


}
