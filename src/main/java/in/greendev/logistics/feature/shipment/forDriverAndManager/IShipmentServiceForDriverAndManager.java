package in.greendev.logistics.feature.shipment.forDriverAndManager;

import in.greendev.logistics.model.Shipment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface IShipmentServiceForDriverAndManager {

    List<Shipment> getShipmentsByShipmentNumberSalesOrder(String number);

    List<Shipment> getShipmentsFromSearchByWord(String word);

    List<Shipment> getShipmentsByShipmentNumberSalesOrderAndDriverName(String number, String driverName);

    Page<Shipment> getAllByPageInCustomDirection(Pageable pageable);

    Page<Shipment> findByLoadingDateBetween(LocalDate startDate, LocalDate endDate, Pageable pageable);

    Page<Shipment> findByAllParameters(List<String> dutyShipmentType, List<String> customerNames, List<String> driverNames, String name, String place, LocalDate startLoading, LocalDate endLoading, LocalDate startUnloading, LocalDate endUnloading, Pageable pageable);


}
