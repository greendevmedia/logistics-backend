package in.greendev.logistics.feature.driver;

import in.greendev.logistics.model.Driver;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IDriverDao extends JpaRepository<Driver, Long> {
}
